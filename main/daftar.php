<?php
//	include_once("../kernel.php");
	session_start();
	include_once("../kernel.php");
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	$msg = '';
	function loadKol()
        {
                $out=null;
                mysql_class::ex_sql("select `name`,`id` from `kol` order by `id`",$q);
                while($r=mysql_fetch_array($q,MYSQL_ASSOC))
                        $out[$r["name"]]=(int)$r["id"];
                return $out;
        }
	function loadRoomCss($css_n='')
	{
		$out = null;
		$class_found = FALSE;
		$css_class = '';
		$css_array = array();
		$lines = file('../css/style.css');
		foreach($lines as $line)
		{
			if(strpos($line,'.room_closed_')!==FALSE)
			{
				$class_found = TRUE;
				$css_class .= $line."\n";
			}
			else if($class_found)
			{
				$css_class .= $line."\n";
				if(strpos($line,'}')!==FALSE)
				{
					$class_found = FALSE;
					$css_array[] = $css_class;
					$css_class = '';
				}
			}
		}
		for($i = 0;$i < count($css_array);$i++)
		{
			$tmp = explode('{',$css_array[$i]);
			$css_name = explode('.',$tmp[0]);
			$css_name = trim($css_name[1]);
			$css_color = '#100000';
			$tmp = explode(';',$tmp[1]);
			for($j=0;$j<count($tmp);$j++)
			{
				$tmp1 = explode(':',$tmp[$j]);
				if(trim($tmp1[0])=='background-color')
					$css_color = trim($tmp1[1]);
			}
			if($css_n == '')
				$out[$css_color] = $css_name;
			else if($css_n == $css_name)
				$out = $css_color;
		}
		return($out);
	}
	function loadColor($inp)
	{
		$out ='&nbsp;';
		mysql_class::ex_sql("select `css_class` from `daftar` where `id` = $inp",$q);
		if($r = mysql_fetch_array($q))
			$out = "<span style=\"background-color:".loadRoomCss($r['css_class']).";\" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
		return($out);
	}
	function loadMoeen($inp)
	{
		$inp = (int)$inp;
		$daftar = new daftar_class($inp);
		if($daftar->sandogh_moeen_id>0)
		{
			$moeen = new moeen_class($daftar->sandogh_moeen_id);
			$nama = $moeen->name.'('.$moeen->code.')';
		}
		else
		{
			$nama = 'انتخاب';
		}
		
		$out = "<u><span onclick=\"window.location =('select_hesab.php?refPage=daftar.php&sel_id=$inp');\"  style='color:blue;cursor:pointer;' >$nama</span></u>";
		return $out;
	}
        function add_item()
        {
                $fields = null;
                foreach($_REQUEST as $key => $value)
                        if(substr($key,0,4)=="new_")
                                if($key != "new_id" )
                                        $fields[substr($key,4)] =perToEnNums($value);
		$kol_id = kol_class::addById($fields['name']);
		$sandogh_moeen_id = moeen_class::addById($kol_id,'صندوق '.$fields['name']);
		$fields['kol_id'] = $kol_id;
		$fields['sandogh_moeen_id'] = $sandogh_moeen_id ;
                $query = '';
                $fi = "(";
                $valu="(";
                foreach ($fields as $field => $value)
                {
                        $fi.="`$field`,";
                        $valu .="'$value',";
                }
                $fi=substr($fi,0,-1);
                $valu=substr($valu,0,-1);
                $fi.=")";
                $valu.=")";
                $query="insert into `daftar` $fi values $valu";
                mysql_class::ex_sqlx($query);
        }
	function delete_item($id)
	{
		$aj = new daftar_class($id);
		if($aj->protected == 1)
			$GLOBALS['msg'] = "<h2 style=\"color:red;\">امکان حذف این دفتر نمی باشد.</h2>";
		else
		{
			mysql_class::ex_sqlx("update `kol` set `name` = CONCAT(`name`,'_پاک‌شده_$id')  where `id` = ".$aj->kol_id."");
			mysql_class::ex_sqlx("update `moeen` set `name` = CONCAT(`name`,'_پاک‌شده_$id')  where `id` = ".$aj->sandogh_moeen_id);
			mysql_class::ex_sqlx("delete from `daftar` where `id` = $id");
		}
	}
	$GLOBALS['msg'] = '';
	$user = new user_class((int)$_SESSION['user_id']);
	if(isset($_REQUEST['sel_id']))
	{
		$moeen_id = (int)$_REQUEST['moeen_id'];
		$sel_id = $_REQUEST['sel_id'];
		$daf = new daftar_class((int)$_REQUEST['sel_id']);
		$q = null;
		$arr = array();
		mysql_class::ex_sql("select `id` from `moeen` where `kol_id`=".$daf->kol_id,$q);
		while($r = mysql_fetch_array($q))
		{
			$arr[] =(int)$r['id'];
		}
		//var_dump($arr);
		if(in_array($moeen_id,$arr))
			mysql_class::ex_sqlx("update `daftar` set `sandogh_moeen_id`=$moeen_id where `id`=$sel_id");
		else
			$msg = '<span style="color:red;">حساب معین انتخاب شده زیر مجموعه کل نیست</span>';
	}
	$grid = new jshowGrid_new("daftar","grid1");
	$grid->index_width = '20px';
	$grid->width = '95%';
	$grid->showAddDefault = FALSE;
	$grid->whereClause="1=1 order by `name`";
	$grid->columnHeaders[0] = null;
	$grid->columnHeaders[1]="نام دفتر";
	$grid->columnHeaders[2]="توضیحات";
	$grid->columnHeaders[3] = "حساب کل";
	$grid->columnLists[3]=loadKol();
	$grid->columnHeaders[4] = "نام کلاس گرافیکی";
	if($conf->is_hesabdari!=='')
		$grid->columnHeaders[4] =null;
	$grid->columnLists[4]=loadRoomCss();
	$grid->columnHeaders[5] =null;
	$grid->columnHeaders[6] = 'تخفیف(درصد)';
	if($conf->is_hesabdari!=='')
		$grid->columnHeaders[6] =null;
	$grid->columnHeaders[7] = (($user->user=='mehrdad')?'PROTECTED':null);
	$grid->addFeild('id');
	$grid->columnHeaders[8] = "حساب معین<br/>صندوق";
	$grid->columnFunctions[8] = 'loadMoeen';
	$grid->columnAccesses[8] = 0;
	$grid->addFeild('id');
	$grid->columnHeaders[9] = "نمونه رنگ";
	if($conf->is_hesabdari!=='')
		$grid->columnHeaders[9] =null;
	$grdi->columnAccesses[9] = 0;
	$grid->columnFunctions[9] = 'loadColor';
	$grid->addFunction = 'add_item';
	$grid->deleteFunction = 'delete_item';
	$grid->intial();
	$grid->executeQuery();
	$grid->canAdd = FALSE;
	if($grid->getRowCount()<$conf->limit_daftar)
		$grid->canAdd = TRUE;
		
	$out = $grid->getGrid();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<link type="text/css" href="../js/jquery/themes/trontastic/jquery-ui.css" rel="stylesheet" />
		<link type="text/css" href="../js/jquery/window/css/jquery.window.css" rel="stylesheet" />

		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<!-- JavaScript Includes -->
		<script type="text/javascript" src="../js/jquery/jquery.js"></script>

		<script type="text/javascript" src="../js/jquery/jquery-ui.js"></script>
		<script type="text/javascript" src="../js/jquery/window/jquery.window.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
مدیریت دفاتر
		</title>
	
		<!--<script type="text/javascript" src="../js/shamsi.js"></script> -->
		<script>
		function st()
		{
		week= new Array("يكشنبه","دوشنبه","سه شنبه","چهارشنبه","پنج شنبه","جمعه","شنبه")
		months = new Array("فروردين","ارديبهشت","خرداد","تير","مرداد","شهريور","مهر","آبان","آذر","دي","بهمن","اسفند");
		a = new Date();
		d= a.getDay();
		day= a.getDate();
		var h=a.getHours();
      		var m=a.getMinutes();
  		var s=a.getSeconds();
		month = a.getMonth()+1;
		year= a.getYear();
		year = (year== 0)?2000:year;
		(year<1000)? (year += 1900):true;
		year -= ( (month < 3) || ((month == 3) && (day < 21)) )? 622:621;
		switch (month) 
		{
			case 1: (day<21)? (month=10, day+=10):(month=11, day-=20); break;
			case 2: (day<20)? (month=11, day+=11):(month=12, day-=19); break;
			case 3: (day<21)? (month=12, day+=9):(month=1, day-=20); break;
			case 4: (day<21)? (month=1, day+=11):(month=2, day-=20); break;
			case 5:
			case 6: (day<22)? (month-=3, day+=10):(month-=2, day-=21); break;
			case 7:
			case 8:
			case 9: (day<23)? (month-=3, day+=9):(month-=2, day-=22); break;
			case 10:(day<23)? (month=7, day+=8):(month=8, day-=22); break;
			case 11:
			case 12:(day<22)? (month-=3, day+=9):(month-=2, day-=21); break;
			default: break;
		}
		//document.write(" "+week[d]+" "+day+" "+months[month-1]+" "+ year+" "+h+":"+m+":"+s);
			var total=" "+week[d]+" "+day+" "+months[month-1]+" "+ year+" "+h+":"+m+":"+s;
			    document.getElementById("tim").innerHTML=total;
   			    setTimeout('st()',500);
		}
		</script>
	</head>
	<body onload='st()'>
		
                <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="right" style="padding-right:30px;padding-top:10px;">
			<a href="help.php" target="_blank"><img src="../img/help.png"/></a>
		</div>
		<div align="center">
			<br/>
			<?php echo $msg.'<br/>'.$GLOBALS['msg']; ?>
			<br/>
			<?php echo $out;  ?>
		</div>
		<script language="javascript" >
			<?php if($conf->hesab_auto){ ?>
			if(document.getElementById('new_kol_id'))
				document.getElementById('new_kol_id').style.display = 'none';
			<?php } ?>
			if(document.getElementById('new_css_class'))
				document.getElementById('new_css_class').style.fontFamily = 'tahoma';
			var inp = document.getElementsByName('new_id');
			for(var i=0;i<inp.length;i++)
				inp[i].style.display = 'none';

			
		</script>
	</body>
		<center>
		<span id='tim' >test2
		</span>
		</center>
</html>
