<?php	session_start();
	include_once("../kernel.php");
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
	if(!$se->can_view)
                die(lang_fa_class::access_deny);
	$GLOBALS['ftyp']='';
	function loadCHange($inp)
	{
		$out = $inp;
		$GLOBALS['ftyp']=$inp;
		$tmp = explode('_',$inp);
		switch ($tmp[0])
		{
			case 'lname':
				$out = 'نام و نام خانوادگی';
				break;
			case 'tedad':
				$out = 'تعداد نفرات';
				break;
			case 'aztarikh':
				$out = 'تاریخ ورود';
				break;
			case 'tatarikh':
				$out = 'تاریخ خروج';
				break;
			case 'shab':
				$out = 'شب رزرو';
				break;
			case 'rooz':
				$out = 'روز رزرو';
				break;
			case 'm':
				switch ($tmp[1])
				{
					case 'hotel':
						$out = 'مبلغ  هتل';
						break;
					case 'belit':
						if($tmp[2]=='1')
							$out = 'مبلغ بلیت رفت';
						else if($tmp[2]=='2')
							$out = 'مبلغ بلیت برگشت';
						break;
				}
				break;
			case 'ajans':
				switch ($tmp[2])
				{
					case '2':
						$out = 'حساب معین بلیت برگشت';
						break;
					case '1':
						$out = 'حساب معین بلیت رفت';
						break;
				}
				break;
			case 'daftar':
				switch ($tmp[2])
				{
					case '2':
						$out = 'حساب کل بلیت برگشت';
						break;
					case '1':
						$out = 'حساب کل بلیت رفت';
						break;
				}
				break;
			case 'otagh':
				$room = new room_class((int)$tmp[1]);
				$out  ='اتاق'.$room->name;
				break;
			case 'khadamat':
				switch ($tmp[1])
				{
					case 'id':
						$khadamat = new khadamat_class((int)$tmp[2]);
						$out = 'خدمات '.$khadamat->name;
						break;
					case 'v':
						$khadamat = new khadamat_class((int)$tmp[2]);
						$out = 'ورودی خدمات '.$khadamat->name;
						break;
					case 'kh':
						$khadamat = new khadamat_class((int)$tmp[2]);
						$out = 'خروجی خدمات '.$khadamat->name;
						break;
				}
				break;

		}
		return $out;
	}
	function loadDate($inp)
	{
		$out=jdate("H:j:s d / m / Y  ",strtotime($inp));
                return $out;
	}
	function loadVal($inp)
	{
		$out = $inp;
		$ftyp = $GLOBALS['ftyp'];
		$tmp = explode('_',$ftyp);
		switch ($tmp[0])
		{
			case 'm':
				$out = monize($inp);
				break;
			case 'ajans':
				$aj = new ajans_class((int)$inp);
				$out = $aj->name;
				break;
			case 'daftar':
				$daf = new daftar_class((int)$inp);
				$out = $daf->name;
				break;
		}
		return $out; 
	}
	function loadUser($inp)
	{
		$user = new user_class((int)$inp);
		$daftar = new daftar_class($user->daftar_id);
		return $user->fname.' '.$user->lname.' ( '.$daftar->name.' ) ';
	}
	$out = '';
        if(isset($_REQUEST['reserve_id']))
	{
		$reserve_id = (int)$_REQUEST['reserve_id'];
		$grid = new jshowGrid_new("changeLog","grid1");
		$grid->whereClause=" `reserve_id`=$reserve_id order by `tarikh`,`id`";
		$grid->pageCount = 20;
		$grid->width = '95%';
		$grid->index_width = '20px';
		$grid->columnHeaders[0] = null;
		$grid->columnHeaders[1] = null;
	       	$grid->columnHeaders[2] ='تغییرات' ;
		$grid->columnFunctions[2] = "loadCHange";
		$grid->columnHeaders[3] = "مقدار قبلی";
		$grid->columnFunctions[3] = "loadVal";
		$grid->columnHeaders[4] = "مقدار اصلاح شده";
		$grid->columnFunctions[4] = "loadVal";
		$grid->columnHeaders[5] = "تاریخ";
		$grid->columnFunctions[5] = "loadDate";
		$grid->columnHeaders[6] = "شماره رزرو";
		$grid->columnHeaders[7] = "کاربر ";
		$grid->columnFunctions[7] = "loadUser";
		$grid->canAdd = FALSE;
		$grid->canDelete = FALSE;
		$grid->canEdit = FALSE;
		$grid->intial();
	   	$grid->executeQuery();
		$out = $grid->getGrid();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<link type="text/css" href="../js/jquery/themes/trontastic/jquery-ui.css" rel="stylesheet" />
		<link type="text/css" href="../js/jquery/window/css/jquery.window.css" rel="stylesheet" />

		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<!-- JavaScript Includes -->
		<script type="text/javascript" src="../js/jquery/jquery.js"></script>

		<script type="text/javascript" src="../js/jquery/jquery-ui.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
		سامانه ارزیابی عملکرد کارکنان شرکت مدیریت تولید نیروگاه‌های گازی خراسان
		</title>
	</head>
	<body>
		<?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="center">
			<br/>
			<br/>
			<?php	echo $out;?>
		</div>
	</body>
</html>
