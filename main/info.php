<?php	session_start();
	include_once("../kernel.php");
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	$out = '';
	if(!isset($_REQUEST['hotel_id']) && (int)$_REQUEST['hotel_id']>0)
		die("<script language=\"javascript\">window.opener.location = window.opener.location;window.close();</script>");
	$hotel_id = (int)$_REQUEST['hotel_id'];
	if(isset($_REQUEST['properties']))
	{
		$tmp['properties'] = str_replace("\n",' ',$_REQUEST['properties']);
		$tmp['profile_link'] = $_REQUEST['profile_link'];
		$tmp['thumbnail_link'] = $_REQUEST['thumbnail_link'];
		$tmp = serialize($tmp);
		mysql_class::ex_sqlx("update `hotel` set `info` = '$tmp' where `id` = $hotel_id");
		die("<script language=\"javascript\">alert('ثبت با موفقیت انجام شد.');window.opener.location = window.opener.location;window.close();</script>");
	}
	$hotel = new hotel_class($hotel_id);
	$properties = isset($hotel->info['properties']) ? $hotel->info['properties'] : '';
	$profile_link = isset($hotel->info['profile_link']) ? $hotel->info['profile_link'] : '';
	$thumbnail_link = isset($hotel->info['thumbnail_link']) ? $hotel->info['thumbnail_link'] : '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<link type="text/css" href="../js/jquery/themes/trontastic/jquery-ui.css" rel="stylesheet" />
		<link type="text/css" href="../js/jquery/window/css/jquery.window.css" rel="stylesheet" />

		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<!-- JavaScript Includes -->
		<script type="text/javascript" src="../js/jquery/jquery.js"></script>

		<script type="text/javascript" src="../js/jquery/jquery-ui.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
		اطلاعات هتل
		</title>
	</head>
	<body>
		<?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="center">
			<br/>
			<br/>
			<form id="frm1">
				<input type="hidden" id="hotel_id" name="hotel_id" value="<?php echo $hotel_id; ?>" />
				<?php echo $hotel->name; ?>
				<table>
					<tr>
						<td>
							مشخصات : 
						</td>
						<td>
							<textarea id="properties" name="properties"><?php echo $properties; ?></textarea>
						</td>
					</tr>
                                        <tr>
						<td>
                                                        لینک پروفایل :
                                                </td>
                                                <td>
                                                        <input class="inp" style="direction:ltr;" type="text" id="profile_link" name="profile_link" value="<?php echo $profile_link; ?>" />
                                                </td>
					</tr>
                                        <tr>
                                                <td>
                                                        لینک عکس کوچک (Thumbnail) هتل :
                                                </td>
                                                <td>
                                                        <input class="inp" style="direction:ltr;" type="text" id="thumbnail_link" name="thumbnail_link" value="<?php echo $thumbnail_link; ?>" />
                                                </td>
					</tr>
                                        <tr>
                                                <td colspan="2" align="center">
                                                        <input class="inp" type="submit" value="ثبت" />
                                                </td>
                                        </tr>
				</table>
			</form>
		</div>
	</body>
</html>
