<?php
	session_start();
	include("../kernel.php");
	include("../simplejson.php");
        if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view || !isset($_REQUEST['room_id']) || !isset($_REQUEST['reserve_id']))
                die(lang_fa_class::access_deny);
	$is_admin = FALSE;
	if($se->detailAuth('all'))
		$is_admin = TRUE;
	function loadQues($ques_id)
	{
		$out = '----';
		$ques_id = (int)$ques_id;
		mysql_class::ex_sql("select name from ravabet_ques where id = $ques_id",$q);
		if($r = mysql_fetch_array($q))
			$out = $r['name'];
		return($out);
	}
	$user_id = (int)$_SESSION['user_id'];
	$GLOBALS['msg'] = '';
	$room_id = (int)$_REQUEST['room_id'];
	$reserve_id = (int)$_REQUEST['reserve_id'];
	$ravabet_id = -1;
	$tarikh = '----';
	mysql_class::ex_sql("select id,tarikh from ravabet where room_id = $room_id and reserve_id = $reserve_id",$q);
	if($r = mysql_fetch_array($q))
	{
		$ravabet_id = (int)$r['id'];
		$tarikh = ($r['tarikh'] != '0000-00-00 00:00:00')?jdate("Y/m/d",strtotime($r['tarikh'])):'----';
	}
	if($ravabet_id == -1)
	{
		$ln = mysql_class::ex_sqlx("insert into ravabet (room_id,reserve_id,tarikh,user_id) values ($room_id,$reserve_id,'$tarikh',$user_id)",FALSE);
		$ravabet_id = mysql_insert_id($ln);
		mysql_close($ln);
		$q = null;
		mysql_class::ex_sql("select id from ravabet_ques order by id",$q);
		$ans = '';
		while($r = mysql_fetch_array($q))
			$ans .= (($ans!='')?' , ':'')."($ravabet_id,".$r['id'].")";
		mysql_class::ex_sqlx("insert into ravabet_det (ravabet_id,ravabet_ques_id) values $ans");
	}
	$grid = new jshowGrid_new("ravabet_det","grid1");
	$grid->width = '99%';
	$grid->index_width = '20px';
	$grid->whereClause = " ravabet_id = $ravabet_id order by `id`";
	$grid->columnHeaders[0] ='';
	$grid->columnHeaders[1] ="";
	$grid->columnHeaders[2] ="سوال";
	$grid->columnAccesses[2] = 0;
	$grid->columnFunctions[2] = "loadQues";
	$grid->columnHeaders[3] ="پاسخ";
	$grid->columnLists[3] = array(
					'بد'=>1,
					'متوسط'=>2,
					'خوب'=>3,
					'عالی'=>4
				);
	$grid->columnAccesses[3] = $se->detailAuth('ravabet') || $is_admin;
	$grid->columnHeaders[4] ="توضیحات";
	$grid->columnAccesses[4] = $se->detailAuth('ravabet') || $is_admin;
	$grid->canAdd = FALSE;
	$grid->canDelete = FALSE;
	$grid->intial();
	$grid->executeQuery();
	$out = $grid->getGrid();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<link type="text/css" href="../js/jquery/themes/trontastic/jquery-ui.css" rel="stylesheet" />
		<link type="text/css" href="../js/jquery/window/css/jquery.window.css" rel="stylesheet" />

		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<!-- JavaScript Includes -->
		<script type="text/javascript" src="../js/jquery/jquery.js"></script>

		<script type="text/javascript" src="../js/jquery/jquery-ui.js"></script>
		<script type="text/javascript" src="../js/jquery/window/jquery.window.js"></script>
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
		سامانه نرم افزاری رزرو آنلاین بهار
		</title>
	</head>
	<body>
		<div align="right" style="padding-right:30px;padding-top:10px;">
			<a href="help.php" target="_blank"><img src="../img/help.png"/></a>
		</div>
		<div align="center">
			<br/>
			<?php echo '<h2>'.$GLOBALS['msg'].'</h2>' ?>
			<br/>
			<?php echo $out;  ?>
		</div>
		<script language="javascript">
			document.getElementById('new_id').style.display = 'none';
		</script>
	</body>
</html>

