<?php
	session_start();
	include_once("../kernel.php");
        if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	$out = '';
	$id = ((isset($_REQUEST['id']))?(int)$_REQUEST['id']:-1);
	$cost_kala_id = ((isset($_REQUEST['cost_kala_id']))?(int)$_REQUEST['cost_kala_id']:-1);
	$cost_tedad = ((isset($_REQUEST['cost_tedad']))?(int)$_REQUEST['cost_tedad']:0);
	if($cost_kala_id>0)
		$cost_kala = new cost_kala_class($cost_kala_id);
	$now = audit_class::hamed_pdate(date("Y-m-d"));
	$anbar_factor = new anbar_factor_class($id);
	$anbar_typ = new anbar_typ_class($anbar_factor->anbar_typ_id);
	$resid = $anbar_typ->name;
	$user = new user_class((int)$_SESSION['user_id']);
	$user = $user->fname.' '.$user->lname;
	$moshtari = new moshtari_class((int)$_SESSION['moshtari_id']);
	//echo $anbar_factor->anbar_typ_id;
	if($anbar_typ->typ==1)
	{
	      $out.='<tr>
						    <th>
						      نام کالا
						    </th>
						    <th>
						      تاریخ
						    </th>
						    <th>
						      تعداد
						    </th>
						    <th>
 واحد
						    </th>
						    <th>
قیمت واحد
						    </th>
						    <th>
قیمت کل
						    </th>
						    <th>
تحویل دهنده
						    </th>
					      </tr>';
	      $ghimat_kol = 0;
	      mysql_class::ex_sql("select * from `anbar_det` where `anbar_factor_id`=$id",$q);
	      while($r = mysql_fetch_array($q))
	      {
		  $kala = new kala_class($r['kala_id']);
		  $tarikh =audit_class::hamed_pdate($r['tarikh']);
		  $vahed = new kala_vahed_class($kala->vahed_id);
		  $ghimat_vahed = ($r['tedad']==0)?'تعریف نشده':monize($r['ghimat']/$r['tedad']);
		  $other_user = new user_class((int)$r['other_user_id']);
		  $other_user = $other_user->fname.' '.$other_user->lname;
		  $ghimat_kol += (int)$r['ghimat'];
		  $out .='<tr>';
		  $out .='<td>'.$kala->name.'</td>';
		  $out .='<td>'.$tarikh.'</td>';
		  $out .='<td>'.(int)$r['tedad'].'</td>';
		  $out .='<td>'.$vahed->name.'</td>';
		  $out .='<td>'.$ghimat_vahed.'</td>';
		  $out .='<td>'.monize($r['ghimat']).'</td>';
		  $out .='<td>'.$other_user.'</td>';
		  $out .="</tr>\n";
	      }
	      $out .="<tr><td colspan='5' style='text-align:left;' >جمع قیمت کل:</td><td>".monize($ghimat_kol)."</td><td>&nbsp;</td></tr>\n";
	}
	else if((int)$anbar_typ->typ==-1)
	{
		$out.='<tr>
						    <th>
						      نام کالا
						    </th>
						    <th>
						      تاریخ
						    </th>
						    <th>
						      تعداد
						    </th>
						    <th>
 واحد
						    </th>

						    <th>
تحویل گیرنده
						    </th>
					      </tr>';
		mysql_class::ex_sql("select * from `anbar_det` where `anbar_factor_id`=$id",$q);
		while($r = mysql_fetch_array($q))
		{
		    $kala = new kala_class($r['kala_id']);
		    $tarikh =audit_class::hamed_pdate($r['tarikh']);
		    $vahed = new kala_vahed_class($kala->vahed_id);
		    $ghimat_vahed = ($r['tedad']==0)?'تعریف نشده':monize($r['ghimat']/$r['tedad']);
		    $other_user = new user_class((int)$r['other_user_id']);
		    $other_user = $other_user->fname.' '.$other_user->lname;
		    $out .='<tr>';
		    $out .='<td>'.$kala->name.'</td>';
		    $out .='<td>'.$tarikh.'</td>';
		    $out .='<td>'.(int)$r['tedad'].'</td>';
		    $out .='<td>'.$vahed->name.'</td>';
		    $out .='<td>'.$other_user.'</td>';
		    $out .="</tr>\n";
		}
	}
?>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link type="text/css" href="../css/style.css" rel="stylesheet" /> 
	<script type="text/javascript" src="../js/tavanir.js"></script>     
	<style>
	td {text-align:center; }
	table.test td {
	border: 1px solid #000000;
}
	</style>
</head>
<body>
        <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
        <div align="center" style="width:18cm;">
		<table border='1' style="width:95%;font-size:14px;height:250px;" cellspacing="0" >
			<tr>
				<th colspan="3" width="80%" >
				<h2><?php echo $moshtari->name; ?></h2><br/>
				</th>
				<td rowspan="2" valign="top" >
					تاریخ چاپ:
						<?php echo $now; ?><br/><br/>
صادر کننده:
						<b><?php echo $user; ?></b>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<?php 
						echo '<h3> رسید '.$resid.'</h3><br/>';
						echo ($cost_kala_id==-1)?'':'جهت '.$cost_tedad.' '.$cost_kala->name;
					?>
				</td>
			</tr>
			<tr>
				<td colspan="4" >
					<table border="1" style="width:100%;font-size:12px;border-width:1px;border-collapse: collapse;" cellspacing="10" >
					      <?php echo $out; ?>
					</table>
				</td>
			</tr>
			<tr height="80px" >
				<td colspan="3" >
					<?php echo $conf->title; ?>
				</td>
				<td style="text-align:right;" rowspan="2">
					امضاء
				</td>
			</tr>
			<tr>
				<td colspan="3" style="text-align:right;font-size:10px;" >
					طراحی و ساخت گستره ارتباطات شرق www.gcom.ir
				</td>
			</tr>
		</table>
        </div>
	<script language="javascript" >
		//window.print();
	</script>
</body>
</html>


