<?php
/*	session_start();
	include_once("../kernel.php");
        if (isset($_SESSION['user_id']) && isset($_SESSION['typ']))
        {
                if (!audit_class::isAdmin($_SESSION['typ']))
                {
                        die("<center><h1>شما به این صفحه دسترسی ندارید</h1></center>");
                }
        }
        else
        {
                        die("<center><h1>شما به این صفحه دسترسی ندارید</h1></center>");
        }*/
	session_start();
        include_once("../kernel.php");
        if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        //var_dump($_SESSION);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	function loadHotels($id = -1)
	{
		$out = "";
		$id = (int)$id;
		mysql_class::ex_sql("select * from `hotel` order by `name`",$q);
		while($r = mysql_fetch_array($q))
		{
			$out .= '<option value="'.(int)$r['id'].'"'.(($id == (int)$r['id'])?'selected="selected"':'').' >';
			$out .= $r['name'];
			$out .= '</option>';
		}
		return($out);
	}
	function hpdate($inp)
	{
		return(audit_class::hamed_pdate($inp));
	}
	function hpdateback($inp)
	{
		return(audit_class::hamed_pdateBack(perToEnNums($inp)));
	}
        function add_item()
        {
                $fields = null;

                foreach($_REQUEST as $key => $value)
                {
                        if(substr($key,0,4)=="new_")
                        {
                                if($key != "new_id" && $key != "new_en" )
                                {
                                        $fields[substr($key,4)] = $value;
                                }
                        }
                }
                if (isset($_REQUEST["hotel_id"]))
                {
                        $hotel_id=$_REQUEST["hotel_id"];
                }
                else
                {
                        $hotel_id=-1;
                }
                $fields["hotel_id"] = $hotel_id;
		$fields["aztarikh"] = audit_class::hamed_pdateBack($fields["aztarikh"]);
		$fields["tatarikh"] = audit_class::hamed_pdateBack($fields["tatarikh"]);
                $fi = "(";
                $valu="(";
                foreach ($fields as $field => $value)
                {
                        $fi.="`$field`,";
                        $valu .="'$value',";
                }
                $fi=substr($fi,0,-1);
                $valu=substr($valu,0,-1);
                $fi.=")";
                $valu.=")";
                $query="insert into `hotel_working_date` $fi values $valu";
                mysql_class::ex_sqlx($query);
        }
	$hotel_id = ((isset($_REQUEST['hotel_id']))?(int)$_REQUEST['hotel_id']:-1);
        $grid = new jshowGrid_new("hotel_working_date","grid1");
	$grid->whereClause=" `hotel_id` = $hotel_id order by `hotel_id`";
        $grid->columnHeaders[0] = null;
        $grid->columnHeaders[1] = null;
	$grid->columnHeaders[2] = "ازتاریخ";
	$grid->columnHeaders[3] = "تاتاریخ";
	$grid->columnHeaders[4] = 'نوع';
	$grid->columnLists[4] = array('غیرپیک'=>0,'پیک'=>1);
	$grid->columnHeaders[5] = 'قیمت';
	$grid->columnFunctions[2] = "hpdate";
	$grid->columnCallBackFunctions[2] = "hpdateback";
        $grid->columnFunctions[3] = "hpdate";
        $grid->columnCallBackFunctions[3] = "hpdateback";
	$grid->addFunction = "add_item";
        $grid->intial();
        $grid->executeQuery();
        $out = $grid->getGrid();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<link type="text/css" href="../js/jquery/themes/trontastic/jquery-ui.css" rel="stylesheet" />
		<link type="text/css" href="../js/jquery/window/css/jquery.window.css" rel="stylesheet" />

		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<!-- JavaScript Includes -->
		<script type="text/javascript" src="../js/jquery/jquery.js"></script>

		<script type="text/javascript" src="../js/jquery/jquery-ui.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
			اطلاعات زمان فعالیت	
		</title>
	</head>
	<body>
		<?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="center">
			<br/>
				<form id="frm1" method="get">
					هتل : <select class="inp" style="width:auto;" name="hotel_id" onchange="document.getElementById('frm1').submit();">
						<?php
							echo loadHotels($hotel_id);
						?>
					</select>
				</form>
			<br/>
			<?php echo $out;
			?>
		</div>
	</body>
</html>
